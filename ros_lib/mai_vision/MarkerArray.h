#ifndef _ROS_mai_vision_MarkerArray_h
#define _ROS_mai_vision_MarkerArray_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Int16MultiArray.h"
#include "geometry_msgs/PoseArray.h"

namespace mai_vision
{

  class MarkerArray : public ros::Msg
  {
    public:
      typedef std_msgs::Int16MultiArray _ids_type;
      _ids_type ids;
      typedef geometry_msgs::PoseArray _pos_type;
      _pos_type pos;

    MarkerArray():
      ids(),
      pos()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->ids.serialize(outbuffer + offset);
      offset += this->pos.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->ids.deserialize(inbuffer + offset);
      offset += this->pos.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return "mai_vision/MarkerArray"; };
    const char * getMD5(){ return "d41305730bd0664658449c5fc7c74560"; };

  };

}
#endif
